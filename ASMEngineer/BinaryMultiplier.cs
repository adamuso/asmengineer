﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ASMEngineer
{
    public enum BinaryMultiplier : int
    {
        NONE = 1,
        KILO = 1 << 10,
        MEGA = 1 << 20,
        GIGA = 1 << 30
    }
}
