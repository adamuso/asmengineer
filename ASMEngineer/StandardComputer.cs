﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ASMEngineer.Parser;

namespace ASMEngineer
{
    public class StandardComputer : Computer
    {
        public StandardComputer()
            : base()
        {
            Parser.setConstant("BYTE", 1);
            Parser.setConstant("byte", 1);
            Parser.setConstant("WORD", 2);
            Parser.setConstant("word", 2);
            Parser.setConstant("DWORD", 4);
            Parser.setConstant("dword", 4);
        }

        public override void Init()
        {
            for (int i = 0; i < 10; i++)
                Parser.setRegister("r" + (i + 1), i);

            for (int i = 10; i < 20; i++)
                Parser.setRegister("a" + (i + 1 - 10), i);

            #region Basic arithmetic operations
            Parser.setInstruction("add", 0x1, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters) 
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;
                    int reg2 = instruction.Parser.Computer.Registers[parameters[1]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 + reg2;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("add", 0x2, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 + parameters[1];
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("mov", 0x3, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    instruction.Parser.Computer.Registers[parameters[0]].Value = instruction.Parser.Computer.Registers[parameters[1]].Value;
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("sub", 0x4, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;
                    int reg2 = instruction.Parser.Computer.Registers[parameters[1]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 - reg2;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("sub", 0x5, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 - parameters[1];
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("mov", 0x6, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    instruction.Parser.Computer.Registers[parameters[0]].Value = parameters[1];
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("mul", 0x7, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;
                    int reg2 = instruction.Parser.Computer.Registers[parameters[1]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 * reg2;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("mul", 0x8, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 * parameters[1];
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("div", 0x9, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;
                    int reg2 = instruction.Parser.Computer.Registers[parameters[1]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 / reg2;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("div", 0xA, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 / parameters[1];
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("mod", 0x27, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 % parameters[1];
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("mod", 0x28, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 % instruction.Parser.Computer.Registers[parameters[1]].Value;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("inc", 0x29, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    instruction.Parser.Computer.Registers[parameters[0]].Value++;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 1, false, ASMParameters.REGISTER);

            Parser.setInstruction("dec", 0x2A, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    instruction.Parser.Computer.Registers[parameters[0]].Value--;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 1, false, ASMParameters.REGISTER);
            #endregion

            #region Bit operations

            Parser.setInstruction("or", 0xB, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;
                    int reg2 = instruction.Parser.Computer.Registers[parameters[1]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 | reg2;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("or", 0xC, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 | parameters[1];
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("and", 0xE, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;
                    int reg2 = instruction.Parser.Computer.Registers[parameters[1]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 & reg2;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("and", 0xF, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 & parameters[1];
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);


            Parser.setInstruction("xor", 0x10, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;
                    int reg2 = instruction.Parser.Computer.Registers[parameters[1]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 ^ reg2;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("xor", 0x11, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 ^ parameters[1];
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("not", 0x12, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = ~reg1;
                }
                ), 2, false, ASMParameters.REGISTER);

            Parser.setInstruction("shr", 0x13, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;
                    int reg2 = instruction.Parser.Computer.Registers[parameters[1]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 >> reg2;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("shl", 0x14, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;
                    int reg2 = instruction.Parser.Computer.Registers[parameters[1]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 << reg2;
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.REGISTER);

            Parser.setInstruction("shr", 0x15, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 >> parameters[1];
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("shl", 0x16, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    instruction.Parser.Computer.Registers[parameters[0]].Value = reg1 << parameters[1];
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);
            #endregion

            #region Basic jumps
            Parser.setInstruction("br", 0x17, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    Registers[31].Value += parameters[0] - 6; // minus size of a branch, which must be constant, otherwise it will not work
                }
                ), 1, true, ASMParameters.POINTER);

            Parser.setInstruction("jmp", 0x18, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    Registers[31].Value = ProgramMemory + parameters[0];
                }
                ), 1, false, ASMParameters.POINTER);
            #endregion

            #region Stack operations
            Parser.setInstruction("push", 0x19, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    StackPush(Registers[parameters[0]]);
                }
                ), 2, false, ASMParameters.REGISTER);

            Parser.setInstruction("pop", 0x1A, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    Registers[parameters[0]].Value = StackPop();
                }
                ), 2, false, ASMParameters.REGISTER);
            #endregion

            #region Conditional jumps, conditions
            Parser.setInstruction("cmp", 0x1B, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    instruction.Parser.Computer.SetFlag(FlagType.EQUAL, Registers[parameters[0]].Value == parameters[1]);
                    instruction.Parser.Computer.SetFlag(FlagType.ZERO, Registers[parameters[0]].Value == 0);
                    instruction.Parser.Computer.SetFlag(FlagType.GREATER, Registers[parameters[0]].Value > parameters[1]);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("je", 0x1C, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    if(instruction.Parser.Computer.GetFlag(FlagType.EQUAL))
                        Registers[31].Value = ProgramMemory + parameters[0];
                }
                ), 1, false, ASMParameters.POINTER);

            Parser.setInstruction("jz", 0x1D, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    if (instruction.Parser.Computer.GetFlag(FlagType.ZERO))
                        Registers[31].Value = ProgramMemory + parameters[0];
                }
                ), 1, false, ASMParameters.POINTER);

            Parser.setInstruction("jne", 0x1E, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    if (!instruction.Parser.Computer.GetFlag(FlagType.EQUAL))
                        Registers[31].Value = ProgramMemory + parameters[0];
                }
                ), 1, false, ASMParameters.POINTER);

            Parser.setInstruction("jnz", 0x1F, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    if (!instruction.Parser.Computer.GetFlag(FlagType.ZERO))
                        Registers[31].Value = ProgramMemory + parameters[0];
                }
                ), 1, false, ASMParameters.POINTER);

            Parser.setInstruction("jgr", 0x20, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    if (instruction.Parser.Computer.GetFlag(FlagType.GREATER))
                        Registers[31].Value = ProgramMemory + parameters[0];
                }
                ), 1, false, ASMParameters.POINTER);

            Parser.setInstruction("jlo", 0x21, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    if (!instruction.Parser.Computer.GetFlag(FlagType.GREATER) && !instruction.Parser.Computer.GetFlag(FlagType.EQUAL))
                        Registers[31].Value = ProgramMemory + parameters[0];
                }
                ), 1, false, ASMParameters.POINTER);

            Parser.setInstruction("jeg", 0x22, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    if (instruction.Parser.Computer.GetFlag(FlagType.GREATER) || instruction.Parser.Computer.GetFlag(FlagType.EQUAL))
                        Registers[31].Value = ProgramMemory + parameters[0];
                }
                ), 1, false, ASMParameters.POINTER);

            Parser.setInstruction("jel", 0x23, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    if (!instruction.Parser.Computer.GetFlag(FlagType.GREATER))
                        Registers[31].Value = ProgramMemory + parameters[0];
                }
                ), 1, false, ASMParameters.POINTER);
            #endregion

            #region Additional operations

            Parser.setInstruction("call", 0x24, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    StackPush(Registers[31].Value);
                    Registers[31].Value = ProgramMemory + parameters[0];
                }
                ), 1, false, ASMParameters.POINTER);

            Parser.setInstruction("ret", 0x25, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    Registers[31].Value = StackPop();
                }
                ), 0, false);

            Parser.setInstruction("end", 0x26, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    instruction.Parser.Computer.CancelProgram();
                }
                ), 0, false);

            Parser.setInstruction("int", 0x2B, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    instruction.Parser.Computer.InterputAction.Invoke(parameters[0]);
                }
                ), 1, false, ASMParameters.VALUE);

            Parser.setInstruction("int", 0x31, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]];

                    instruction.Parser.Computer.InterputAction.Invoke(reg1);
                }
                ), 1, false, ASMParameters.REGISTER);
            #endregion

            #region Memory operations

            Parser.setInstruction("mov", 0x2C, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    if (parameters[2] == 1)
                        instruction.Parser.Computer.Registers[parameters[0]].Value = Memory.read<byte>(parameters[1]);
                    else if (parameters[2] == 2)
                        instruction.Parser.Computer.Registers[parameters[0]].Value = Memory.read<short>(parameters[1]);
                    else if (parameters[2] == 4)
                        instruction.Parser.Computer.Registers[parameters[0]].Value = Memory.read<int>(parameters[1]);
                }
                ), 3, false, ASMParameters.REGISTER, ASMParameters.POINTER, ASMParameters.VALUE);


            Parser.setInstruction("mov", 0x2D, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    if (parameters[2] == 1)
                        Memory.write<byte>(parameters[0], (byte)instruction.Parser.Computer.Registers[parameters[1]].Value);
                    else if (parameters[2] == 2)
                        Memory.write<short>(parameters[0], (short)instruction.Parser.Computer.Registers[parameters[1]].Value);
                    else if (parameters[2] == 4)
                        Memory.write<int>(parameters[0], instruction.Parser.Computer.Registers[parameters[1]].Value);
                }
                ), 3, false, ASMParameters.POINTER, ASMParameters.REGISTER, ASMParameters.VALUE);

            Parser.setInstruction("mov", 0x2E, new InstructionAction(
               delegate(ASMInstruction instruction, int[] parameters)
               {
                   if (parameters[2] == 1)
                       Memory.write<byte>(parameters[0], (byte)parameters[1]);
                   else if (parameters[2] == 2)
                       Memory.write<short>(parameters[0], (short)parameters[1]);
                   else if (parameters[2] == 4)
                       Memory.write<int>(parameters[0], parameters[1]);
               }
               ), 3, false, ASMParameters.POINTER, ASMParameters.VALUE, ASMParameters.VALUE);

            Parser.setInstruction("mov", 0x2F, new InstructionAction(
               delegate(ASMInstruction instruction, int[] parameters)
               {
                    Memory.write<int>(parameters[0], instruction.Parser.Computer.Registers[parameters[1]]);
               }
               ), 2, false, ASMParameters.POINTER, ASMParameters.REGISTER);

            Parser.setInstruction("mov", 0x30, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    instruction.Parser.Computer.Registers[parameters[0]].Value = Memory.read<int>(parameters[1]);
                }
                ), 2, false, ASMParameters.REGISTER, ASMParameters.POINTER);
            #endregion

            Parser.setInstruction("print", 0x32, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    int reg1 = instruction.Parser.Computer.Registers[parameters[0]].Value;

                    Console.WriteLine(reg1);
                }
                ), 1, false, ASMParameters.REGISTER);

            Parser.setInstruction("print", 0x33, new InstructionAction(
                delegate(ASMInstruction instruction, int[] parameters)
                {
                    string s = "";
                    int pt = parameters[0];

                    while (Memory.read<byte>(pt) != 0)
                    {
                        s += (char)Memory.read<byte>(pt);
                        pt += 1;
                    }

                    Console.WriteLine(s);
                }
                ), 1, false, ASMParameters.POINTER);
        }

        public override void InitMemory(out int program_memory_address, out int stack_memory_address, out int stack_size)
        {
            program_memory_address = 0;
            stack_size = 1024;
            stack_memory_address = Memory.MemorySize - (stack_size * 4); 
        }

        public override void Update()
        {

        }
    }
}
