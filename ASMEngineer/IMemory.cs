﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ASMEngineer
{
    public interface IMemory : IDisposable
    {
        byte read(Pointer address);
        T read<T>(Pointer address) where T : struct;
        byte[] read(Pointer address, int len);
        void write(Pointer address, byte value);
        void write<T>(Pointer address, T value) where T : struct;
        void write(Pointer address, byte[] values);
        int MemorySize { get; }
    }
}
