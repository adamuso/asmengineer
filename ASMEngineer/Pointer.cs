﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ASMEngineer
{
    public struct Pointer
    {
        ulong value;

        public Pointer(ulong value)
        {
            this.value = value;
        }

        public static implicit operator long(Pointer p)
        {
            return (long)p.value;
        }

        public static implicit operator Pointer(ulong p)
        {
            return new Pointer(p) ;
        }

        public static implicit operator Pointer(long p)
        {
            return new Pointer((ulong)p);
        }

        public static implicit operator Pointer(uint p)
        {
            return new Pointer(p);
        }

        public static implicit operator Pointer(int p)
        {
            return new Pointer((ulong)p);
        }

        public static implicit operator Pointer(ushort p)
        {
            return new Pointer(p);
        }

        public static implicit operator Pointer(short p)
        {
            return new Pointer((ulong)p);
        }
    }
}
