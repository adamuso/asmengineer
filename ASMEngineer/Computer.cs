﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ASMEngineer.Parser;

namespace ASMEngineer
{
    public delegate void InterputAction(int interput);

    public abstract class Computer
    {
        public const int WORD_SIZE = 32;
        public const int REGISTER_BITS = 5;
        public const int NUMBER_OF_REGISTERS = 1 << REGISTER_BITS;

        int ID = new Random((int)DateTime.Now.Ticks).Next(int.MaxValue);

        bool cancel, running;
        Register<int>[] registers;
        Memory memory;
        Parser.ASMParser parser;
        Dictionary<byte, Parser.ASMInstruction> opcodes;
        int flags;
        InterputAction interputAction;
        System.IO.StreamWriter stdOut;

        int program_memory;
        int stack_memory;
        int stack_size;

        public Computer()
        {
            this.registers = new Register<int>[NUMBER_OF_REGISTERS];
            this.opcodes = new Dictionary<byte, Parser.ASMInstruction>();
            this.interputAction = null;
            this.stdOut = null;

            for (int r = 0; r < NUMBER_OF_REGISTERS; r++)
                this.registers[r] = new Register<int>();

            SetMemory(8, BinaryMultiplier.KILO);

            this.parser = new Parser.ASMParser(this);
            Init();

            Parser.setRegister("sp", 30);
            Parser.setRegister("pc", 31);
        }

        internal void AddInstruction(byte opcode, Parser.ASMInstruction instruction)
        {
            opcodes[opcode] = instruction;
        }

        public void RegisterInterputAction(InterputAction action)
        {
            interputAction = action;
        }

        public void SetMemory(int size, BinaryMultiplier multiplier)
        {
            if(memory != null)
                memory.Dispose();

            memory = new Memory(size, multiplier);
            InitMemory(out program_memory, out stack_memory, out stack_size);

            registers[30].Value = stack_memory;
            registers[31].Value = program_memory;
        }

        public void SetStackMemoryStart(int address)
        {
            stack_memory = address;
            registers[30].Value = stack_memory;
        }

        public void StackPush(int value)
        {
            memory.write<int>(registers[30].Value, value);
            registers[30].Value++;

            if (registers[30].Value > stack_memory + stack_size * 4)
                throw new Exception("Stack overflow!");
        }

        public int StackPop()
        {
            registers[30].Value--;

            if (registers[30].Value < stack_memory)
                throw new Exception("Stack underflow!");

            return memory.read<int>(registers[30].Value);
        }

        public void Run()
        {
            running = true;
        }

        internal void RunProgram()
        {
            Register<int> current = registers[31];
            running = true;
            //current.Value = program_memory;
            int instruction = -1;

            while (instruction != 0)
            {
                instruction = memory.read<int>((ushort)current.Value);

                if (instruction == 0 || instruction == -1)
                {
                    running = false;
                    break;
                }

                byte opcode = memory.read<byte>((ushort)current.Value);
                byte paramstypes = memory.read<byte>((ushort)current.Value + 1);
                int mem = current.Value + 2;

                Parser.ASMInstruction asm = opcodes[opcode];
                int[] param = new int[asm.Parameters];

                for (int i = 0; i < asm.Parameters; i++)
                {
                    byte pt = (byte)((paramstypes & (0x03 << i * 2)) >> i * 2);

                    if (asm.ParametersType[i] == ASMParameters.REGISTER)
                    {
                        if (pt == 0x00)
                        {
                            param[i] = memory.read<byte>(mem);
                            mem++;
                        }
                    }
                    else if (asm.ParametersType[i] == ASMParameters.VALUE)
                    {
                        if (pt == 0x00)
                        {
                            param[i] = memory.read<byte>(mem);
                            mem++;
                        }
                        else if (pt == 0x01)
                        {
                            param[i] = memory.read<short>(mem);
                            mem += 2;
                        }
                        else if (pt == 0x02)
                        {
                            param[i] = memory.read<int>(mem);
                            mem += 4;
                        }
                    }
                    else if (asm.ParametersType[i] == ASMParameters.POINTER)
                    {
                        if (pt == 0x00)
                        {
                            param[i] = memory.read<int>(mem);
                            mem += 4;
                        }
                        else if (pt == 0x01)
                        {
                            param[i] = Registers[memory.read<byte>(mem)].Value;
                            mem += 1;
                        }
                    }
                }

                current.Value = mem;

                asm.eval(param);

                if (cancel)
                {
                    cancel = false;
                    running = false;
                    break;
                }
            }
        }

        public bool Step()
        {
            running = true;
            Register<int> current = registers[31];

            int instruction = memory.read<int>((ushort)current.Value);

            if (instruction == 0 || instruction == -1)
            {
                running = false;
                return false;
            }

            byte opcode = memory.read<byte>((ushort)current.Value);
            byte paramstypes = memory.read<byte>((ushort)current.Value + 1);
            int mem = current.Value + 2;

            Parser.ASMInstruction asm = opcodes[opcode];
            int[] param = new int[asm.Parameters];

            for (int i = 0; i < asm.Parameters; i++)
            {
                byte pt = (byte)((paramstypes & (0x03 << i * 2)) >> i * 2);

                if (asm.ParametersType[i] == ASMParameters.REGISTER)
                {
                    if (pt == 0x00)
                    {
                        param[i] = memory.read<byte>(mem);
                        mem++;
                    }
                }
                else if (asm.ParametersType[i] == ASMParameters.VALUE)
                {
                    if (pt == 0x00)
                    {
                        param[i] = memory.read<byte>(mem);
                        mem++;
                    }
                    else if (pt == 0x01)
                    {
                        param[i] = memory.read<short>(mem);
                        mem += 2;
                    }
                    else if (pt == 0x02)
                    {
                        param[i] = memory.read<int>(mem);
                        mem += 4;
                    }
                }
                else if (asm.ParametersType[i] == ASMParameters.POINTER)
                {
                    if (pt == 0x00)
                    {
                        param[i] = memory.read<int>(mem);
                        mem += 4;
                    }
                    else if (pt == 0x01)
                    {
                        param[i] = Registers[memory.read<byte>(mem)].Value;
                        mem += 1;
                    }
                }
            }

            current.Value = mem;

            asm.eval(param);

            if (cancel)
            {
                cancel = false;
                running = false;
                return false;
            }

            return true;
        }

        public void CancelProgram()
        {
            cancel = true;
        }

        public void ResetComputer()
        {

        }

        public void Compile(string text)
        {
            parser.compile(text);
        }

        public void Compile(System.IO.Stream stream)
        {
            parser.compile(stream);

            OutLine("Compiled succesfully!");
        }

        internal int DecodeAddress(int undecode)
        {
            int minus = undecode & 0x1;

            if (minus == 1)
                return -(undecode >> 1);

            return undecode >> 1;
        }

        internal bool GetFlag(FlagType type)
        {
            return (flags & (0x1 << (int)type)) != 0;
        }

        internal void SetFlag(FlagType type, bool val)
        {
            if (val)
                flags |= (0x1 << (int)type);
            else
                flags &= ~(0x1 << (int)type);
        }

        internal void OutLine(string text)
        {
            if (stdOut == null)
                return;

            if (!stdOut.BaseStream.CanWrite)
                return;

            stdOut.WriteLine("[COMPUTER: " + Convert.ToString(ID, 16) + "] " + text);
            stdOut.Flush();
        }

        public abstract void Init();
        public abstract void InitMemory(out int program_memory_address, out int stack_memory_address, out int stack_size);
        public abstract void Update();

        internal Dictionary<byte, Parser.ASMInstruction> Instructions { get { return opcodes; } }
        public Register<int>[] Registers { get { return registers; } }
        public IMemory Memory { get { return memory; } }
        internal Memory BaseMemory { get { return memory; } }
        internal Parser.ASMParser Parser { get { return parser; } }
        public int ProgramMemory { get { return program_memory; } }
        internal InterputAction InterputAction { get { return interputAction; } }
        public bool Running { get { return running; } }
        public System.IO.Stream StandardOutput { set { stdOut = new System.IO.StreamWriter(value); } }
    }
}
