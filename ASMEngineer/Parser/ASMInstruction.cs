﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ASMEngineer.Parser
{
    public delegate void InstructionAction(ASMInstruction instruction,  params int[] _params);

    public class ASMInstruction
    {
        ASMParser parser;
        InstructionAction action;
        ASMParameters[] parameters;
        bool label_offset;

        public ASMInstruction(ASMParser parser, InstructionAction action, bool label_offset = true)
        {
            this.parser = parser;
            this.action = action;
            this.label_offset = label_offset;
            parameters = new ASMParameters[0];
        }

        public ASMInstruction setParameters(params ASMParameters[] parameters)
        {
            this.parameters = parameters;
            return this;
        }

        public void eval(params int[] param)
        {
            if (param.Length == Parameters)
                action.DynamicInvoke(this, param);
            else
                throw new ArgumentOutOfRangeException("This method is only applicable for " + Parameters + " arguments!");
        }

        public ASMParser Parser { get { return parser; } }
        public int Parameters { get { return parameters.Length; } }
        public ASMParameters[] ParametersType { get { return parameters; } }
        public bool LabelOffset { get { return label_offset; } }
    }
}
