﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ASMEngineer.Parser
{
    public class ASMOpcodeSet
    {
        Computer computer;
        List<byte> opcodes;
        byte _default;

        public ASMOpcodeSet(Computer computer)
        {
            this.computer = computer;
            this.opcodes = new List<byte>();
        }

        public void Add(byte opcode)
        {
            opcodes.Add(opcode);
        }

        public byte Find(params ASMParameters[] parameters)
        {
            for (int i = 0; i < opcodes.Count; i++)
            {
                ASMInstruction ins = computer.Instructions[opcodes[i]];

                if (ins.Parameters == parameters.Length)
                {
                    bool found = true;

                    for (int j = 0; j < parameters.Length; j++)
                    {
                        if ((parameters[j] & ASMParameters.POINTER) == 0)
                        {
                            if (parameters[j] != ins.ParametersType[j])
                            {
                                found = false;
                                break;
                            }
                        }
                        else
                        {
                            if (ASMParameters.POINTER != ins.ParametersType[j])
                            {
                                found = false;
                                break;
                            }
                        }
                    }

                    if (!found)
                        continue;
                    else
                        return opcodes[i];
                }
                else
                    continue;
            }

            return 255;
        }

        public int Count { get { return opcodes.Count; } }
        public List<byte> Opcodes { get { return opcodes; } }
        public byte Default { get { return _default; } set { _default = value; } }
    }
}
